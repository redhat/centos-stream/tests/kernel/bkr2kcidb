"""Test Find Compose Pkg"""

from importlib.resources import files
import io
import itertools
import json
import pathlib
import unittest
from unittest import mock

import responses

from kernel_qe_tools.ci_tools import find_compose_pkg

ASSETS = files(__package__) / 'assets/find_compose_pkg'


class TestFindComposePkg(unittest.TestCase):
    """Test Find Compose Pkg."""

    def setUp(self):
        """Init class."""
        rhel_rpms_file = pathlib.Path(ASSETS, 'RHEL-8.10.0-20240312.89_rpms.json')
        rhel_rpms_content = rhel_rpms_file.read_text(encoding='utf-8')
        self.rhel_rpms_content_json = json.loads(rhel_rpms_content)

        # Daily compose, two successful requests
        self.daily_compose = 'RHEL-8.10.0-20240312.89'
        daily_cts_compose_url = (
            f'{find_compose_pkg.CTS_API_URL}/{self.daily_compose}'
        )
        responses.add(responses.GET, daily_cts_compose_url, status=200,
                      json={'compose_url': 'http://composeserver.com/compose_1'})

        daily_compose_rpms_url = (
            'http://composeserver.com/compose_1/compose/metadata/rpms.json'
        )
        responses.add(responses.GET, daily_compose_rpms_url, status=200,
                      json=self.rhel_rpms_content_json)

        # Daily compose not found, one request, URL not found
        self.daily_compose_not_found = 'RHEL-8.8.0-20230411.10'
        non_found_cts_compose_url = (
            f'{find_compose_pkg.CTS_API_URL}/{self.daily_compose_not_found}'
        )
        responses.add(responses.GET, non_found_cts_compose_url, status=404)

        # Daily compose fetching error, one request, internal server error
        # Error at CTS
        self.daily_compose_fetch_error_with_cts = 'RHEL-8.8.0-20230411.12'
        self.daily_compose_fetch_error_with_cts_url = (
            f'{find_compose_pkg.CTS_API_URL}/{self.daily_compose_fetch_error_with_cts}'
        )
        responses.add(responses.GET, self.daily_compose_fetch_error_with_cts_url, status=500)

        # Daily compose fetching error, two requests, internal server error
        # Error at download server
        self.daily_compose_fetch_error_downloading_rpms = 'RHEL-8.8.0-20230411.13'
        self.daily_compose_fetch_error_downloading_rpms_url = (
            f'{find_compose_pkg.CTS_API_URL}/{self.daily_compose_fetch_error_downloading_rpms}'
        )
        responses.add(responses.GET, self.daily_compose_fetch_error_downloading_rpms_url,
                      status=200,
                      json={'compose_url': 'http://composeserver.com/compose_2'})
        daily_compose_fetch_error_downloading_rpms_url = (
            'http://composeserver.com/compose_2/compose/metadata/rpms.json'
        )
        responses.add(responses.GET, daily_compose_fetch_error_downloading_rpms_url, status=500)

        # Released compose, one successful request
        self.released_compose = 'RHEL-8.8.0'
        released_compose_rpms_url = (
            'http://download.devel.redhat.com/released/RHEL-8/'
            f'{self.released_compose.replace("RHEL-", "")}/metadata/rpms.json'
        )
        responses.add(responses.GET, released_compose_rpms_url, status=200,
                      json=self.rhel_rpms_content_json)

        # Released compose not found, one request, URL not found
        self.released_compose_not_found = 'RHEL-8.9.0'
        released_compose_rpms_url_not_found = (
            'http://download.devel.redhat.com/released/RHEL-8/'
            f'{self.released_compose_not_found.replace("RHEL-", "")}/metadata/rpms.json'
        )
        responses.add(responses.GET, released_compose_rpms_url_not_found, status=404)

        # Release compose fetching error, one request, internal server error
        self.released_compose_fetching_error = 'RHEL-8.5.0'
        released_compose_rpms_url_fetching_error = (
            'http://download.devel.redhat.com/released/RHEL-8/'
            f'{self.released_compose_fetching_error.replace("RHEL-", "")}/metadata/rpms.json'
        )
        responses.add(responses.GET, released_compose_rpms_url_fetching_error, status=500)

    def test_find_pkgs(self):
        """Ensure find_pkgs works."""
        arches = {'aarch64', 'ppc64le', 's390x', 'x86_64'}
        expected_kernel = [{
                'arches': ['aarch64', 'ppc64le', 's390x', 'x86_64'],
                'nvr': 'kernel-4.18.0-544.el8',
                'variant': 'BaseOS'
            },
            {
                'arches': ['x86_64'],
                'nvr': 'kernel-4.18.0-544.el8',
                'variant': 'CRB'
        }]
        expected_rtla = [{
            'arches': ['aarch64', 'ppc64le', 's390x', 'x86_64'],
            'nvr': 'rtla-6.6.0-1.el8',
            'variant': 'AppStream'
        }]
        all_togheter = list(itertools.chain(expected_kernel, expected_rtla))
        rpm_info = self.rhel_rpms_content_json['payload']['rpms']
        cases = (
            ('Only one package, found it', {'kernel'}, expected_kernel),
            ('Only one package, found it without arch', {'rtla'}, expected_rtla),
            ('Multiple packages, found all packages', {'kernel', 'rtla'}, all_togheter),
            ('Multiple packages, found some packages', {'kernel', 'rtla', 'random-package'},
             all_togheter),
            ('No packages found', {'random-package'}, []),
        )
        for description, packages, expected_output in cases:
            with self.subTest(description):
                result = find_compose_pkg.find_pkgs(packages, rpm_info)
                self.assertCountEqual(expected_output, result['packages'])
                self.assertCountEqual(arches, result['compose_arches'])

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_print_packages_info(self, mock_stdout):
        """Ensure print_package_info works."""
        compose_arches = {'aarch64', 'ppc64le', 's390x', 'x86_64'}
        one_package_with_all_arches = {
            'compose_arches': compose_arches,
            'packages': [{
                'arches': ['aarch64', 'ppc64le', 's390x', 'x86_64'],
                'nvr': 'rtla-6.6.0-1.el8',
                'variant': 'AppStream'
            }]
        }
        one_package_with_one_arch = {
            'compose_arches': compose_arches,
            'packages': [{
                'arches': ['x86_64'],
                'nvr': 'kernel-4.18.0-544.el8',
                'variant': 'BaseOS'
            }]
        }
        one_package_multiples_variants = {
            'compose_arches': compose_arches,
            'packages': [{
                'arches': ['aarch64', 'ppc64le', 's390x', 'x86_64'],
                'nvr': 'kernel-4.18.0-544.el8',
                'variant': 'BaseOS'
                },
                {
                'arches': ['x86_64'],
                'available_arches': ['aarch64', 'ppc64le', 's390x', 'x86_64'],
                'nvr': 'kernel-4.18.0-544.el8',
                'variant': 'CRB'
                }]
        }
        cases = (
                ('No verbosity one package, one variant', one_package_with_all_arches,
                 0, 'rtla-6.6.0-1.el8'),
                ('No verbosity one package, multiples variant', one_package_multiples_variants,
                 0, 'kernel-4.18.0-544.el8'),
                ('Verbosity one package, with all arches', one_package_with_all_arches,
                 1, ['rtla-6.6.0-1.el8', 'AppStream: rtla-6.6.0-1.el8']),
                ('Verbosity one package, with one arch', one_package_with_one_arch,
                 1, ['kernel-4.18.0-544.el8', 'BaseOS: kernel-4.18.0-544.el8']),
                ('Verbosity one package, multiples variant', one_package_multiples_variants,
                 1, ['kernel-4.18.0-544.el8', 'BaseOS: kernel-4.18.0-544.el8',
                     'CRB: kernel-4.18.0-544.el8']),
        )
        for description, packages, verbose, expected_output in cases:
            with self.subTest(description):
                find_compose_pkg.print_packages_info(packages, verbose)
                if isinstance(expected_output, list):
                    for output in expected_output:
                        self.assertIn(output, mock_stdout.getvalue())
                else:
                    self.assertIn(expected_output, mock_stdout.getvalue())

    @responses.activate
    def test_get_compose_info_with_valid_composes(self):
        """Ensure get_compose_info works."""
        cases = (
            ('Get compose info from a daily compose', self.daily_compose),
            ('Get compose info from a released compose', self.released_compose),
        )

        verbose = 0
        for (description, compose) in cases:
            with self.subTest(description):
                self.assertDictEqual(self.rhel_rpms_content_json,
                                     find_compose_pkg.get_compose_info(compose, verbose))

    @responses.activate
    def test_get_compose_info_check_errors(self):
        """Ensure get_compose_info with errors."""
        verbose = 0
        cases = (
            ('Get compose info from a daily compose not found',
             self.daily_compose_not_found, find_compose_pkg.ComposeNotFound),
            ('Get compose info from a released compose not found',
             self.released_compose_not_found, find_compose_pkg.ComposeNotFound),
            ('Get compose info with a fetch error in a released compose',
             self.released_compose_fetching_error, find_compose_pkg.FetchError),
            ('Get compose info with a fetch error in a daily compose',
             self.daily_compose_fetch_error_with_cts, find_compose_pkg.FetchError),
            ('Get compose info with a fetch error downloading rpms in a daily compose',
             self.daily_compose_fetch_error_downloading_rpms, find_compose_pkg.FetchError),
        )

        for (description, compose, exception) in cases:
            with self.subTest(description):
                self.assertRaises(exception, find_compose_pkg.get_compose_info, compose, verbose)

    @responses.activate
    def test_main_exit_codes(self):
        """Ensure main returns the right exit code."""
        cases = (
            ('Package found in a daily compose',
             ['-c', self.daily_compose],
             find_compose_pkg.EXIT_PACKAGE_FOUND),
            ('Package found in a released compose',
             ['-c', self.released_compose],
             find_compose_pkg.EXIT_PACKAGE_FOUND),
            ('Package not found in a daily compose',
             ['-c', self.daily_compose, '-p', 'random-package'],
             find_compose_pkg.EXIT_PACKAGE_NOT_FOUND),
            ('Package not found in a released compose',
             ['-c', self.released_compose, '-p', 'random-package'],
             find_compose_pkg.EXIT_PACKAGE_NOT_FOUND),
            ('Daily compose not found',
             ['-c', self.daily_compose_not_found],
             find_compose_pkg.EXIT_COMPOSE_NOT_FOUND),
            ('Released compose not found',
             ['-c', self.released_compose_not_found],
             find_compose_pkg.EXIT_COMPOSE_NOT_FOUND),
            ('Fetch error with a released compose',
             ['-c', self.released_compose_fetching_error],
             find_compose_pkg.EXIT_FETCH_ERROR),
            ('Fetch error with a daily compose',
             ['-c', self.daily_compose_fetch_error_with_cts],
             find_compose_pkg.EXIT_FETCH_ERROR),
            ('Fetch error downloading rpms with a daily compose',
             ['-c', self.daily_compose_fetch_error_downloading_rpms],
             find_compose_pkg.EXIT_FETCH_ERROR),
            ('Error using parameter', [],
             find_compose_pkg.EXIT_INCORRECT_USAGE),
        )

        for (description, args, exit_code) in cases:
            with self.subTest(description):
                self.assertEqual(exit_code,
                                 find_compose_pkg.main(args)
                                 )

    @responses.activate
    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_find_compose_pkg_main_valid(self, mock_stdout):
        """Smoke test with a valid compose."""
        result = find_compose_pkg.main(['-c', self.released_compose, '-p', 'kernel-rt', '-v'])
        self.assertEqual(result, 0)
        self.assertIn("NFV: kernel-rt-4.18.0-544.rt7.333.el8 ['x86_64']",
                      mock_stdout.getvalue())
        self.assertIn("RT: kernel-rt-4.18.0-544.rt7.333.el8 ['x86_64']",
                      mock_stdout.getvalue())

    @responses.activate
    def test_main_stdout_without_verbosity(self):
        """Ensure rhel package stdout works without verbosity."""
        cases = (
            ('Get kernel from a compose (default option)',
             ['-c', self.daily_compose],
             'kernel-4.18.0-544.el8'),
            ('Get a package from a compose (gcc)',
             ['-c', self.released_compose, '-p', 'gcc'],
             'gcc-8.5.0-21.el8'),
            ('Get multiples packages from a compose (kernel, rtla)',
             ['-c', self.daily_compose, '-p', 'kernel', '-p', 'rtla'],
             ['kernel-4.18.0-544.el8', 'rtla-6.6.0-1.el8']),
            ('Get multiples packages from a compose (rtla, random-package)',
             ['-c', self.released_compose, '-p', 'rtla', '-p', 'random-package'],
             'rtla-6.6.0-1.el8'),
            ('Get a non exist package (random-package)',
             ['-c', self.daily_compose, '-p', 'random-package'],
             ''),
        )
        for (description, args, expected_output) in cases:
            with self.subTest(description), mock.patch(
                'sys.stdout', new_callable=io.StringIO
            ) as mock_stdout:
                output = find_compose_pkg.main(args)
                if isinstance(expected_output, list):
                    for output in expected_output:
                        self.assertIn(output, mock_stdout.getvalue())
                else:
                    if expected_output != '':
                        self.assertIn(expected_output, mock_stdout.getvalue())
                    else:
                        self.assertEqual('', mock_stdout.getvalue())

    @responses.activate
    def test_main_stdout_with_verbosity(self):
        """Ensure rhel package stdout works with verbosity."""
        cases = (
            ('Get compose ID from a released compose',
             ['-c', self.released_compose, '-v'],
             'RHEL-8.10.0-20240312.89'),
            ('Get kernel from a compose (default option)',
             ['-c', self.daily_compose, '-v'],
             ['BaseOS: kernel-4.18.0-544.el8', 'CRB: kernel-4.18.0-544.el8']),
            ('Get a package from a compose (gcc) presents in all arches',
             ['-c', self.daily_compose, '-p', 'gcc', '-v'],
             ['AppStream: gcc-8.5.0-21.el8', 'BaseOS: gcc-8.5.0-21.el8', 'CRB: gcc-8.5.0-21.el8']),
            ('Get a package from a compose (rt-tests) not presents in all arche',
             ['-c', self.daily_compose, '-p', 'rt-tests', '-v'],
             "AppStream: rt-tests-2.6-3.el8 ['x86_64']"),
            ('Get a package in some variant and not present in all arches',
             ['-c', self.released_compose, '-p', 'kernel-rt', '-v'],
             ["NFV: kernel-rt-4.18.0-544.rt7.333.el8 ['x86_64']",
              "RT: kernel-rt-4.18.0-544.rt7.333.el8 ['x86_64']"
              ]),
            ('Show a message when the package is not found',
             ['-c', self.daily_compose, '-p', 'random-package', '-v'],
             "Package/s ['random-package'] not found in compose RHEL-8.10.0-20240312.89"),
            ('Show a message when the compose is not found',
             ['-c', self.daily_compose_not_found, '-v'],
             'Compose RHEL-8.8.0-20230411.10 not found'),
            ('Show an error when fetching information',
             ['-c', self.daily_compose_fetch_error_with_cts, '-v'],
             'Error fetching compose/package/s info'),
        )
        for (description, args, expected_output) in cases:
            with self.subTest(description), mock.patch(
                'sys.stdout', new_callable=io.StringIO
            ) as mock_stdout:
                output = find_compose_pkg.main(args)
                if isinstance(expected_output, list):
                    for output in expected_output:
                        self.assertIn(output, mock_stdout.getvalue())
                else:
                    if expected_output != '':
                        self.assertIn(expected_output, mock_stdout.getvalue())
                    else:
                        self.assertEqual('', mock_stdout.getvalue())

    def test_get_nvr_from_name(self):
        """Ensure get_nvr_from_name works."""
        cases = (
            ('Get NVR from a package',
             'kernel', 'kernel-0:4.18.0-544.el8.src', 'kernel-4.18.0-544.el8'),
            ('Get NVR from a kernel-rt (kernel is a similar name)',
             'kernel-rt', 'kernel-rt-0:4.18.0-544.rt7.333.el8.src',
             'kernel-rt-4.18.0-544.rt7.333.el8'),
            ('Get NVR from a non matching pacakge',
             'kernel', 'gcc-0:8.5.0-21.el8.src', None),
        )
        for (description, package, src_rpm, expected_output) in cases:
            with self.subTest(description):
                self.assertEqual(expected_output,
                                 find_compose_pkg.get_nvr_from_name(package, src_rpm))
