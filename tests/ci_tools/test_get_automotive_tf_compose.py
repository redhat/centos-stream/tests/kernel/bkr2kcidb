"""Test Get Automotive TF Compose."""

import io
import unittest
from unittest import mock

from kernel_qe_tools.ci_tools import get_automotive_tf_compose

MOCKED_GET_COMPOSE = ('kernel_qe_tools.ci_tools.get_automotive_tf_compose.get_auto_compose.'
                      'calculate_auto_compose')

MOCKED_GET_COMPOSE_ID = ('kernel_qe_tools.ci_tools.get_automotive_tf_compose.get_auto_compose.'
                         'get_auto_compose_id')


class TestCLI(unittest.TestCase):
    """Test Automotive Compose CLI."""

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_cli_without_default_parameter(self, stderr_mock):
        """Test CLI without default parameter."""
        expected_message = ("error: the following arguments are required: "
                            "--webserver-releases, --release")
        with self.assertRaises(SystemExit) as context:
            get_automotive_tf_compose.main([])
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_cli_without_get_compose_or_get_compose_id(self, stderr_mock):
        """Test CLI without default parameter."""
        expected_message = ("error: one of the arguments --get-compose "
                            "--get-compose-id is required")
        args = (
            "--webserver-releases latest "
            "--release latest "
        ).split()

        with self.assertRaises(SystemExit) as context:
            get_automotive_tf_compose.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch(MOCKED_GET_COMPOSE)
    def test_cli_get_compose_with_mininum_parameter(self, get_auto_compose_mock):
        """Test CLI with minimum parameter."""
        expected_params = {
            'ARCH': 'aarch64',
            'RELEASE_NAME': 'latest',
            'IMAGE_NAME': 'qa',
            'IMAGE_TYPE': 'regular',
            'HW_TARGET': 'ridesx4',
            'webserver_releases': 'latest'
        }
        args = (
            "--webserver-releases latest "
            "--release latest "
            "--get-compose "
        ).split()
        get_automotive_tf_compose.main(args)
        get_auto_compose_mock.assert_called_once_with(expected_params)

    @mock.patch(MOCKED_GET_COMPOSE_ID)
    def test_cli_get_compose_id_with_mininum_parameter(self, get_auto_compose_id_mock):
        """Test CLI with minimum parameter."""
        expected_params = ['http_server', 'latest']
        args = (
            "--webserver-releases http_server "
            "--release latest "
            "--get-compose-id "
        ).split()
        get_automotive_tf_compose.main(args)
        get_auto_compose_id_mock.assert_called_once_with(*expected_params)

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    @mock.patch(MOCKED_GET_COMPOSE)
    def test_when_the_compose_is_found(self, get_auto_compose_mock, stdout_mock):
        """Test when the compose is found."""
        expected_code = 0
        expected_message = 'compose text'
        args = (
            "--webserver-releases latest "
            "--release latest "
            "--get-compose "
        ).split()
        # We don't care about the first element of the tuple
        get_auto_compose_mock.return_value = ('foo', expected_message)
        return_code = get_automotive_tf_compose.main(args)
        get_auto_compose_mock.assert_called_once()
        self.assertEqual(expected_code, return_code)
        self.assertIn(expected_message, stdout_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    @mock.patch(MOCKED_GET_COMPOSE)
    def test_when_the_compose_is_not_found(self, get_auto_compose_mock, stderr_mock):
        """Test when the compose is not found."""
        expected_code = 1
        expected_message = 'Unable to get the compose'
        args = (
            "--webserver-releases latest "
            "--release latest "
            "--get-compose "
        ).split()
        get_auto_compose_mock.return_value = None
        return_code = get_automotive_tf_compose.main(args)
        get_auto_compose_mock.assert_called_once()
        self.assertEqual(expected_code, return_code)
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    @mock.patch(MOCKED_GET_COMPOSE_ID)
    def test_when_the_compose_id_is_not_found(self, get_auto_compose_id_mock, stderr_mock):
        """Test when the compose is not found."""
        expected_code = 1
        expected_message = 'Unable to get the compose ID'
        args = (
            "--webserver-releases latest "
            "--release latest "
            "--get-compose-id "
        ).split()
        get_auto_compose_id_mock.return_value = None
        return_code = get_automotive_tf_compose.main(args)
        get_auto_compose_id_mock.assert_called_once()
        self.assertEqual(expected_code, return_code)
        self.assertIn(expected_message, stderr_mock.getvalue())

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    @mock.patch(MOCKED_GET_COMPOSE_ID)
    def test_when_the_compose_id_is_found(self, get_auto_compose_id_mock, stdout_mock):
        """Test when the compose is found."""
        expected_code = 0
        expected_message = 'compose id'
        args = (
            "--webserver-releases latest "
            "--release latest "
            "--get-compose-id "
        ).split()
        # We don't care about the first element of the tuple
        get_auto_compose_id_mock.return_value = 'compose id'
        return_code = get_automotive_tf_compose.main(args)
        get_auto_compose_id_mock.assert_called_once()
        self.assertEqual(expected_code, return_code)
        self.assertIn(expected_message, stdout_mock.getvalue())
