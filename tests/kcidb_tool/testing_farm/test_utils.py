"""Test testing farm utils."""

from importlib.resources import files
import pathlib
import unittest
import xml.etree.ElementTree as ET

from kernel_qe_tools.kcidb_tool.testing_farm import utils

ASSETS = files(__package__) / 'assets'


class TestUtil(unittest.TestCase):
    """Test case for utils."""

    def test_get_kcidb_test_status(self):
        """Ensure get_kcidb_test_status works."""
        cases = (
            ('undefined goes to ERROR', 'undefined', 'ERROR'),
            ('error goes to ERROR', 'error', 'ERROR'),
            ('passed goes to PASS', 'passed', 'PASS'),
            ('failed goes to FAIL', 'failed', 'FAIL'),
            ('info goes to PASS', 'info', 'PASS'),
            ('not_applicable goes to SKIP', 'not_applicable', 'SKIP'),
            ('skipped goes to SKIP', 'skipped', 'SKIP'),
            ('needs_inspection goes to FAIL', 'needs_inspection', 'ERROR'),
        )
        for description, testing_farm_result, expected in cases:
            with self.subTest(description):
                self.assertEqual(expected, utils.get_kcidb_test_status(testing_farm_result))

    def test_get_kcidb_result_status(self):
        """Ensure get_kcidb_result_status works."""
        self.assertEqual('RESULT', utils.get_kcidb_result_status('result'))

    def test_get_arch_from_suite(self):
        """Ensure get_arch_from_suite works."""
        with_arch = """
        <testsuite>
          <testing-environment name="requested">
            <property name="arch" value="x86_64"/>
          </testing-environment>
        </testsuite>
        """

        without_arch = "<testsuite/>"

        cases = (
            ('With arch', with_arch, 'x86_64'),
            ('Without arch', without_arch, None),
        )

        for description, xml_content, expected in cases:
            with self.subTest(description):
                test_suite = ET.fromstring(xml_content)
                self.assertEqual(expected, utils.get_arch_from_testsuite(test_suite))

    def test_get_hostname_from_testcase(self):
        """Ensure get_hostname_from_testcase works."""
        with_hostname = """
        <testcase>
          <properties>
            <property name="baseosci.connectable_host" value="10.31.10.185"/>
          </properties>
        </testcase>
        """

        without_hostname = """
        <testcase>
          <properties>
            <property name="baseosci.arch" value="x86_64"/>
          </properties>
        </testcase>
        """

        cases = (
            ('With hostname', with_hostname, '10.31.10.185'),
            ('Without hostname', without_hostname, None),
        )

        for description, xml_content, expected in cases:
            with self.subTest(description):
                test_case = ET.fromstring(xml_content)
                self.assertEqual(expected, utils.get_hostname_from_testcase(test_case))

    def test_get_results(self):
        """Ensure get_results works."""
        xml_content = """
        <checks checks="1" errors="0" failures="0">
          <check name="dmesg" result="pass" event="before-test">
            <logs>
              <log href="http://artifacts.server/log_file.txt" name="log_file.txt"/>
            </logs>
          </check>
        </checks>
        """
        test = {'id': 'test_id'}
        expected = [{
            'id': 'test_id.2',
            'name': 'dmesg',
            'comment': 'dmesg',
            'status': 'PASS',
            'output_files': [{
                'name': 'log_file.txt',
                'url': 'http://artifacts.server/log_file.txt'
            }]
        }]
        results = ET.fromstring(xml_content)
        self.assertListEqual(expected, utils.get_results(test, results, 2))

    def test_create_virtual_result(self):
        """Ensure create_virtual_result works."""
        test = {
            'id': 'test_id',
            'path': 'test_name',
            'status': 'PASS',
            'output_files': [{
                'name': 'testout.log',
                'url': 'http://artifacts.server/output.txt'
            }]
        }
        expected = {
            'id': 'test_id.1',
            'name': 'test_name',
            'comment': 'test_name',
            'output_files': [{
                'name': 'testout.log',
                'url': 'http://artifacts.server/output.txt'
            }],
            'status': 'PASS'
        }
        self.assertDictEqual(expected, utils.create_virtual_result(test))

    def test_create_system_provision_testcase_when_testsuite_has_testcases(self):
        """Test when everything is ok."""
        tmt_content = pathlib.Path(ASSETS,
                                   'testing_farm_system_provision.xml').read_text(encoding='utf-8')
        root = ET.fromstring(tmt_content)
        test_suite = root.findall('.//testsuite')[1]

        test_case = utils.create_system_provision_test_case(test_suite, 1)

        checks = test_case.findall('.//check')

        self.assertEqual('system_provision', test_case.get('name'))
        self.assertEqual('1', test_case.get('id'))
        self.assertEqual('0', test_case.get('time'))
        self.assertEqual(10, len(test_case.findall('./logs/log')))
        self.assertEqual(1, len(checks))
        self.assertEqual('system_provision', checks[0].get('name'))
        self.assertEqual('pass', checks[0].get('result'))
        self.assertEqual(10, len(checks[0].findall('.//log')))
        self.assertEqual('passed', test_case.get('result'))
        self.assertEqual('True', test_case.get('fake'))

    def test_create_system_provision_testcase_when_testsuite_does_not_have_testcases(self):
        """Test when provision fails."""
        tmt_content = pathlib.Path(ASSETS,
                                   'testing_farm_system_provision.xml').read_text(encoding='utf-8')
        root = ET.fromstring(tmt_content)
        test_suite = root.findall('.//testsuite')[0]

        test_case = utils.create_system_provision_test_case(test_suite, 1)

        checks = test_case.findall('.//check')

        self.assertEqual('system_provision', test_case.get('name'))
        self.assertEqual('1', test_case.get('id'))
        self.assertEqual('0', test_case.get('time'))
        self.assertEqual(10, len(test_case.findall('./logs/log')))
        self.assertEqual(1, len(checks))
        self.assertEqual('system_provision', checks[0].get('name'))
        self.assertEqual('error', checks[0].get('result'))
        self.assertEqual(10, len(checks[0].findall('.//log')))
        self.assertEqual('error', test_case.get('result'))
        self.assertEqual('True', test_case.get('fake'))

    def test_get_job_url(self):
        """Ensure get_job_url works."""
        canonical_logs = """
        <xml>
          <logs>
            <log href="http://testing-farm.server/job/1234/logs/log.txt" name="log.txt"/>
            <log href="http://testing-farm.server/job/1234" name="workdir"/>
          </logs>
        </xml>
        """
        canonical_logs_ending_with_slash = """
        <xml>
          <logs>
            <log href="http://testing-farm.server/job/1234/logs/log.txt" name="log.txt"/>
            <log href="http://testing-farm.server/job/1234/" name="workdir"/>
          </logs>
        </xml>
        """
        logs_without_workdir = """
        <xml>
          <logs>
            <log href="http://testing-farm.server/job/1234/logs/log.txt" name="log.txt"/>
          </logs>
        </xml>
        """
        logs_without_any_log = """
        <xml>
          <logs/>
        </xml>
        """
        no_logs = """
        <xml/>
        """
        expected = 'http://testing-farm.server/job'
        cases = (
            ('Normal case', canonical_logs, expected),
            ('Canonical logs ending with slash', canonical_logs_ending_with_slash, expected),
            ('Logs without workdir', logs_without_workdir, None),
            ('Logs without any log', logs_without_any_log, None),
            ('No logs', no_logs, None),
        )

        for description, xml_content, expected in cases:
            with self.subTest(description):
                test_suite = ET.fromstring(xml_content)
                self.assertEqual(expected, utils.get_job_url(test_suite.find('logs')))
