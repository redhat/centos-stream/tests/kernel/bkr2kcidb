"""Find Compose Package."""

import argparse
import itertools
import re
import sys
import typing

from cki_lib.session import get_session
from requests.exceptions import HTTPError

# CTS API URL
CTS_API_URL = 'https://cts.engineering.redhat.com/api/1/composes'

# Download server (released RHEL)
DOWNLOAD_SERVER = "http://download.devel.redhat.com"

# Define exit codes
EXIT_PACKAGE_FOUND: typing.Final[int] = 0
EXIT_PACKAGE_NOT_FOUND: typing.Final[int] = 1
EXIT_COMPOSE_NOT_FOUND: typing.Final[int] = 2
EXIT_FETCH_ERROR: typing.Final[int] = 3
EXIT_INCORRECT_USAGE: typing.Final[int] = 4

JSON_HEADERS = {'Accept': 'application/json'}
SESSION = get_session(__name__, headers=JSON_HEADERS, raise_for_status=True)


# Declare some exceptions, to make code simpler
class FetchError(Exception):
    """Error fetching compose/package info."""


class ComposeNotFound(Exception):
    """Compose ID not found."""


def usage():
    """Print usage information."""
    print("""
  Usage: find_compose_pkg.py -c <compose> -p <package(s)> [-v]

  Given a RHEL compose, this script will attempt to find the
  NVR of any package(s) provided.

  -c <compose> is mandatory.  If no -p <package(s)> is/are provided,
  then 'kernel' will be used by default.

  If -v is supplied, verbose output will be enabled, which will print
  all repositories the package is found in, including any arch
  limitations.  Without -v, just the first package name match will be
  printed (suitable for automation).

  The script will exit with:
    0 --> package(s) found in compose
    1 --> package(s) cannot be found in compose
    2 --> compose ID cannot be found
    3 --> any error fetching compose/package info
    4 --> incorrect usage
          """)


def get_nvr_from_name(name: str, src_rpm: str) -> str | None:
    """
    Get the NVR from the name given a source rpm name.

    For example, given the name 'kernel-rt' and the source rpm
    kernel-rt-0:4.18.0-544.rt7.333.el8.src, the NVR will be
    'kernel-rt-4.18.0-544.rt7.333.el8'.

    We need to get the package name from the source rpm, which is
    everything before to the last dash before the colon, followed by a number.

    If this matches the name, we can get the NVR by getting everything after the colon
    and removing the '.src' at the end.

    Args:
        name: The name.
        src_rpm: The source rpm.

    Returns:
        The NVR or None.
    """
    match_package = re.match(r'(.+)-\d+:.+\.src', src_rpm)
    if match_package and name == match_package.group(1):
        version_release = re.match(r'.+:(.+).src$', src_rpm).group(1)
        return f'{name}-{version_release}'
    return None


def find_pkgs(pkg_list: typing.Set[typing.Any],
              compose_rpms: typing.Dict[typing.Any, typing.Any]
              ) -> typing.Dict[typing.Any, typing.Any]:
    """Find all instances of each package in pkg_list in the compose."""
    results: dict = {'compose_arches': set(), 'packages': []}

    for pkg, (variant, variant_values) in itertools.product(pkg_list, compose_rpms.items()):
        if variant in ["BaseOS", "AppStream", "RT", "NFV", "HighAvailability",
                       "CRB", "Server", "Server-RT", "RHIVOS"]:
            variant_result = {'arches': [], 'nvr': None, 'variant': variant}

            for arch in variant_values:
                results['compose_arches'].add(arch)
                for src in variant_values[arch]:
                    if nvr := get_nvr_from_name(pkg, src):
                        variant_result['nvr'] = nvr
                        variant_result['arches'].append(arch)
                        break

            # If we find package's info, add it to results
            if variant_result['nvr'] is not None:
                results['packages'].append(variant_result)

    return results


def is_a_released_rhel_version(compose: str, verbose: int) -> bool:
    """Check if the compose is a released RHEL version."""
    valid_rhel_regex = r'^RHEL-\d{1,2}\.\d{1,2}\.\d{1,2}$'
    if verbose > 1:
        print(f"Checking if {compose} is a released RHEL version")
    return bool(re.match(valid_rhel_regex, compose))


def get_info_from_a_rhel_version_release(compose: str,
                                         verbose: int) -> typing.Dict[typing.Any, typing.Any]:
    """Get the information from a released RHEL version."""
    version = compose.replace('RHEL-', '')
    major = "RHEL-" + version.split('.')[0]
    url = f"{DOWNLOAD_SERVER}/released/{major}/{version}/metadata/rpms.json"
    try:
        if verbose > 1:
            print(f'Getting rpms from: {url}')
        info = SESSION.get(url).json()
        if verbose > 0:
            print(info['payload']['compose']['id'])
        return info

    except HTTPError as err:
        if verbose > 1:
            print(err)
        if err.response.status_code == 404:
            if verbose > 1:
                print(f'Released compose {compose} not found at {url}')
            raise ComposeNotFound from err
        if verbose > 1:
            print(f'Error fetching relased compose {compose} info at {url}')
        raise FetchError from err


def get_info_from_a_compose_id(compose: str, verbose: int) -> typing.Dict[typing.Any, typing.Any]:
    """Get the information from a compose ID."""
    # We need to get the download url for this compose
    url = f"{CTS_API_URL}/{compose}"
    try:
        if verbose > 1:
            print(f'Getting compose info from: {url}')
        cts_info = SESSION.get(url).json()
    except HTTPError as err:
        if verbose > 1:
            print(err)
        if err.response.status_code == 404:
            if verbose > 1:
                print(f'Compose ID {compose} not found at {url}')
            raise ComposeNotFound from err
        if verbose > 1:
            print(f'Error fetching relased compose {compose} info {url}')
        raise FetchError from err
    # Now we can get the rpms.json file
    compose_info = f'{cts_info["compose_url"]}/compose/metadata/rpms.json'
    try:
        if verbose > 1:
            print(f'Getting rpms from: {compose_info}')
        return SESSION.get(compose_info).json()
    except HTTPError as err:
        if verbose > 1:
            print(err)
            print(f'Error fetching packages info for {compose} info {compose_info}')
        raise FetchError from err


def print_packages_info(packages_info: dict[typing.Any, typing.Any], verbose: int) -> None:
    """Print the packages info."""
    if verbose > 0:
        len_available_arches = len(packages_info['compose_arches'])
        for pkg in packages_info['packages']:
            if len(pkg['arches']) == len_available_arches:
                print(f"{pkg['variant']}: {pkg['nvr']}")
            else:
                print(f"{pkg['variant']}: {pkg['nvr']} {pkg['arches']}")
    else:
        # Only print once per nvr
        for pkg in {pkg['nvr'] for pkg in packages_info['packages']}:
            print(pkg)


def get_compose_info(compose: str, verbose: int) -> typing.Dict[typing.Any, typing.Any]:
    """
    Fetch the information from a compose.

    If the compose is a released RHEL version, the information will be fetched from the download
    server. Otherwise, the information will be fetched from the CTS API.

    Args:
        compose: The compose.
        verbose: Enable verbose output.

    Returns:
        The compose information.
    """
    if is_a_released_rhel_version(compose, verbose):
        if verbose > 1:
            print(f"Checking released RHEL version {compose}")
        return get_info_from_a_rhel_version_release(compose, verbose)
    if verbose > 1:
        print(f"Checking compose ID {compose}")
    return get_info_from_a_compose_id(compose, verbose)


def main(args: typing.Optional[typing.List[str]] = None) -> int:
    """Command line interface for find_compose_pkg."""
    description = 'Find the package NVR for a package name within a RHEL compose'

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('-c', '--compose', help='RHEL compose ID')
    parser.add_argument('-p', '--package', help='RHEL package(s), may be provided multiple times',
                        action='append', dest='packages')
    parser.add_argument('-v', '--verbose', help='Enable verbose printing',
                        action='count', default=0)

    parsed_args = parser.parse_args(args)

    if parsed_args.compose is None:
        usage()
        return EXIT_INCORRECT_USAGE

    packages_list = set(parsed_args.packages or ['kernel'])

    try:
        compose_info = get_compose_info(parsed_args.compose, parsed_args.verbose)
    except FetchError:
        if parsed_args.verbose > 0:
            print("Error fetching compose/package/s info")
        return EXIT_FETCH_ERROR
    except ComposeNotFound:
        if parsed_args.verbose > 0:
            print(f"Compose {parsed_args.compose} not found")
        return EXIT_COMPOSE_NOT_FOUND
    packages_info = find_pkgs(packages_list, compose_info['payload']['rpms'])
    if packages_info['packages']:
        print_packages_info(packages_info, parsed_args.verbose)
        return EXIT_PACKAGE_FOUND
    if parsed_args.verbose > 0:
        print(f'Package/s {list(packages_list)} not found in compose {parsed_args.compose}')
    return EXIT_PACKAGE_NOT_FOUND


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main(sys.argv[1:]))
