"""Common utils from kcidb_tool."""

import argparse
from email.utils import parseaddr
import json
import pathlib
import re
from urllib.parse import urlparse

EMAIL_RE = r"([A-Za-z0-9]+[.\-_]?)*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+"


class ActionNotFound(Exception):
    """Raised when an action is not found."""


def clean_dict(data):
    """Remove keys with empty value from a dict."""
    return {
        key: value
        for key, value in data.items()
        if isinstance(value, bool) or value
    }


def slugify(string):
    """
    Slugify a string.

    Rules:

    Valid characters:
    * numbers
    * letters
    * underscores
    * dashes

    Replace other characters by underscore.
    Replace consecutive underscores with a single one.
    Remove leading and trailing underscores.
    Remove leading and trailing dashes.
    """
    # Replace invalid characters by underscores
    string = re.sub(r'[^a-zA-Z0-9-_]', '_', string)
    # Replace consecutive underscores with a single underscore
    string = re.sub(r'_+', '_', string)
    # Remove leading dashes
    string = re.sub(r'^[-_]+', '', string)
    # Remove trailing dashes
    string = re.sub(r'[-_]+$', '', string)
    return string


def get_int(text):
    """Cast to int when it's possible."""
    try:
        return int(text)
    except (ValueError, TypeError):
        return None


def get_nvr(nvr):
    """Get a tuple with name, release and version of the package, or return Nones if invalid."""
    try:
        name, version, release = nvr.rsplit('-', 2)
        return name, version, release
    except ValueError:
        return None, None, None


def get_nvr_info(nvr):
    """
    Old version of get_nvr_info.

    We need to keep a while until seque will be updated.

    Get a tuple with name removing '-debug-' if it's present,
    and release and version together, or return Nones if invalid.
    """
    name, version, release = get_nvr(nvr)
    if name:
        return name.replace('-debug', ''), f'{version}-{release}'
    return None, None


def get_provenance_info(function, url, service_name=None, misc=None):
    """Get provenance info."""
    if url:
        return clean_dict({
            'function': function,
            'url': url,
            'service_name': service_name or get_service_name_by_url(url),
            'misc': misc,
        })
    return {}


def get_brew_url(brew_id):
    """Get brew url."""
    return f'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID={brew_id}'


def email_type(value):
    """Check an email address."""
    _, address = parseaddr(value)
    if not re.fullmatch(EMAIL_RE, address):
        raise argparse.ArgumentTypeError(f"'{value}' is not a valid email address.")
    return value


def report_rules_type(value):
    """Check if report_rules is a valid json."""
    try:
        json.loads(value)
    except json.JSONDecodeError as e:
        raise argparse.ArgumentTypeError(f'{value} is not a valid JSON, error: {e}')
    return value


def get_output_files(logs, names_list=None):
    """
    Return output_files from a log section.

    If a list of names is provided, it only get log matching the given name.
    """
    output_files = []
    if logs is not None:
        for log in logs.iter('log'):
            # Fallback to restraint's "filename" and "path" to support users running it locally
            log_name = pathlib.Path(log.get("name") or log.get("filename")).name
            url = log.get("href") or (
                # Path needs to be converted into a proper URI
                (path := log.get("path")) and f"file://{pathlib.Path(path).resolve()}"
            )
            if not names_list or log_name in names_list:
                output_files.append({"name": log_name, "url": url})
    return output_files


def get_service_name_by_url(url):
    """Get service name by URL."""
    service_name = 'Unknown'
    parsed_url = urlparse(url)
    hostname = parsed_url.hostname.lower()
    path = parsed_url.path.lower()
    if 'jenkins' in hostname:
        service_name = 'jenkins'
    elif 'gitlab' in hostname:
        service_name = 'gitlab'
    elif 'osci' in hostname and path.startswith('/testing-farm'):
        service_name = 'testing-farm'
    return service_name
