"""Subcommand create."""

import pathlib
import sys

from cki_lib import misc
from cki_lib.yaml import ValidationError

from . import cmd_misc
from . import utils
from .bkr.parser import Bkr2KCIDBParser
from .dataclasses import ExternalOutputFile
from .dataclasses import NVR
from .dataclasses import ParserArguments
from .testing_farm.parser import TestingFarmParser


def build(cmds_parser, common_parser):
    """Build the argument parser for the create command."""
    cmd_parser, _ = cmd_misc.build(
        cmds_parser,
        common_parser,
        "create",
        help_message='Convert a Beaker or TMT results file into kcidb format.',
        add_subparser=False,
    )

    cmd_parser.description = 'Convert Beaker or TMT results file into kcidb format.'

    cmd_parser.add_argument('-c', '--checkout',
                            type=str,
                            required=True,
                            help="The checkout name used to generate all the kcidb ids.")

    cmd_parser.add_argument('--contact',
                            type=utils.email_type,
                            required=True,
                            action='append',
                            help='Contact email, "Full Name <username@domain>" and username@domain '
                                 'are accepted, this argument can be used several times.')

    cmd_parser.add_argument('--nvr',
                            type=str,
                            required=True,
                            help="NVR info.")

    cmd_parser.add_argument('--source',
                            type=str,
                            choices=['beaker', 'testing-farm'],
                            required=True,
                            help="Source of the original result file (beaker or testing-farm)..")

    cmd_parser.add_argument('-a', '--add-output-files',
                            nargs='+',
                            action=misc.StoreNameValuePair,
                            metavar="NAME=URL",
                            help="Add a list of output files to every test, the syntax is name=url "
                                 "(example: job_url=https://jenkins/job/my_job/1)"
                            )

    cmd_parser.add_argument('--brew-task-id',
                            type=str,
                            required=False,
                            help="Id of the Brew task where the package was compiled.")

    cmd_parser.add_argument('-d', '--debug',
                            action='store_true',
                            help="Enable it if using kernel debug build."
                            " DEPRECATED, kcidb_tool will get the information from the nvr.")

    cmd_parser.add_argument('-i', '--input',
                            type=str,
                            required=True,
                            help="Path to the original result file.")

    cmd_parser.add_argument('-o', '--output',
                            type=str,
                            default='kcidb.json',
                            help="Path to the KCIDB file (By default kcidb.json).")

    cmd_parser.add_argument('--origin',
                            type=str,
                            required=False,
                            default='kcidb_tool',
                            help="The default origin for all objects (By default kcidb_tool)."
                            " It can be overwritten with builds-origin, checkout-origin or"
                            " tests-origin")

    cmd_parser.add_argument('--checkout-origin',
                            type=str,
                            required=False,
                            help="The origin for the checkout, if it's not provided will"
                            " fallback to origin")

    cmd_parser.add_argument('--builds-origin',
                            type=str,
                            required=False,
                            help="The origin for all builds, if it's not provided will"
                            " fallback to origin")

    cmd_parser.add_argument('--tests-origin',
                            type=str,
                            required=False,
                            help="The origin for all tests, if it's not provided will"
                            " fallback to origin")

    cmd_parser.add_argument('-t', '--test_plan',
                            type=bool,
                            default=False,
                            help="(DEPRECATED) Generate only test_plan, please use the subcommand"
                            " create-test-plan.")

    cmd_parser.add_argument('--tests-provisioner-url',
                            type=str,
                            required=False,
                            help="URL of the tests provisioner, usually jenkins job url.")

    cmd_parser.add_argument('--src-nvr',
                            type=str,
                            required=False,
                            help="NVR for the source package, nvr value will be used if it is not"
                            " provided"
                            )

    cmd_parser.add_argument('--submitter',
                            type=utils.email_type,
                            required=False,
                            help="Email of the person who submitted the job.")

    cmd_parser.add_argument('--report-rules',
                            type=utils.report_rules_type,
                            required=False,
                            help="An string with the rules to send reports, must be JSON blob and"
                            " it's used by CKI")


def main(args):
    """Run cli command."""
    beaker_content = pathlib.Path(args.input).read_text(encoding='utf-8')
    if args.add_output_files:
        extra_output_files = [ExternalOutputFile(name=_name, url=_url)
                              for _name, _url in args.add_output_files.items()]
    else:
        extra_output_files = []
    package_name, package_version, package_release = utils.get_nvr(args.nvr)
    if None in [package_name, package_version, package_release]:
        sys.exit(f'Invalid value for nvr: {args.nvr}')

    if args.debug and not package_name.endswith('-debug'):
        package_name += '-debug'

    src_nvr = args.src_nvr or args.nvr
    src_package_name, src_package_version, src_package_release = utils.get_nvr(src_nvr)
    if None in [src_package_name, src_package_version, src_package_release]:
        sys.exit(f'Invalid value for source nvr: {src_nvr}')

    external_arguments = ParserArguments(
        brew_task_id=args.brew_task_id,
        builds_origin=args.builds_origin or args.origin,
        checkout=args.checkout,
        checkout_origin=args.checkout_origin or args.origin,
        contacts=args.contact,
        extra_output_files=extra_output_files,
        nvr=NVR(package_name, package_version, package_release),
        src_nvr=NVR(src_package_name, src_package_version, src_package_release),
        test_plan=args.test_plan,
        tests_origin=args.tests_origin or args.origin,
        tests_provisioner_url=args.tests_provisioner_url,
        report_rules=args.report_rules,
        submitter=args.submitter
    )

    if args.test_plan:
        print('Warning: The --test_plan option is deprecated, please use the subcommand '
              'create-test-plan instead')

    if args.source == 'beaker':
        parser = Bkr2KCIDBParser(beaker_content, external_arguments)

    if args.source == 'testing-farm':
        parser = TestingFarmParser(beaker_content, external_arguments)

    parser.process()
    try:
        parser.write(args.output)
    except ValidationError as exc:
        sys.exit(str(exc))

    if not parser.has_tests():
        sys.exit('The parser did not get any tests')

    print(f'File {args.output} wrote !!')
