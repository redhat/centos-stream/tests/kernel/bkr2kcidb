# Get compose info for a testing farm request related with automotive

## Getting started

The tool will query the automotive datasource to get the compose information related to a testing
farm request.

## Usage

```bash
get_automotive_tf_compose --help
usage: get_automotive_tf_compose [-h] [--arch {aarch64,x86_64}] [--hw-target {am69sk,aws,j784s4evm,qemu,rcar_s4,ridesx4,rpi4,s32g_vnp_rdb3}] --webserver-releases
                                 WEBSERVER_RELEASES --release RELEASE [--image-type {ostree,regular}] [--image-name {cki,developer,minimal,qa,qm,qm-minimal}]

Get the automotive compose for Testing Farm.

options:
  -h, --help            show this help message and exit
  --arch {aarch64,x86_64}
                        Arch (By default aarch64).
  --hw-target {am69sk,aws,j784s4evm,qemu,rcar_s4,ridesx4,rpi4,s32g_vnp_rdb3}
                        HW Target (By default ridesx4).
  --webserver-releases WEBSERVER_RELEASES
                        Webserver releases URL.
  --release RELEASE     Release Name.
  --image-type {ostree,regular}
                        Image Type (By default regular).
  --image-name {cki,developer,minimal,qa,qm,qm-minimal}
                        Image name (By default qa).
```

## Testing

From the repository root, run tox tests:
`podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production tox`
