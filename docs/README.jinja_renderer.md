# `kernel_qe_hw.ci_tools.jinja_renderer.md` - An opinionated Jinja renderer

This tool uses `yaml` or `JSON` file/s to provide the data, and a Jinja template.

## Help

```shell
jinja_renderer --help
usage: jinja_renderer [-h] --input INPUT_FILES --template TEMPLATE [--output OUTPUT]

Another opinionated Jinja Renderer. You can provided a YAML or JSON files to provide data, and Jinja 2 template file.

options:
  -h, --help            show this help message and exit
  --input INPUT_FILES, -i INPUT_FILES
                        Input file (YAML or JSON), this paramter can be invoke several times.
  --template TEMPLATE, -t TEMPLATE
                        Template file.
  --output OUTPUT, -o OUTPUT
                        Output file, otherwise standard output.
```
