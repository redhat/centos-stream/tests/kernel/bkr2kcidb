# Send test result to OSCI via UMB

## Getting started

The tool will query brew for details about the build and submit a test result using UMB.

## Usage

```bash
export KOJI_SERVER="${brew_hub_url}"

usage: result2osci.py [-h] --source-nvr SOURCE_NVR --log-url LOG_URL --run-url RUN_URL --pipeline-id PIPELINE_ID [--test-category CATEGORY] [--test-namespace NAMESPACE]
                      [--test-type TYPE] --test-status {complete,error,running} [--test-result {passed,failed}] --certificate CERTIFICATE [--dry-run]

Create a message to be sent to test.complete topic as OSCI gating message.

options:
  -h, --help            show this help message and exit
  --source-nvr SOURCE_NVR
                        Source package NVR.
  --log-url LOG_URL     log of the test run.
  --run-url RUN_URL     URL for the test run.
  --pipeline-id PIPELINE_ID
                        Unique ID for the pipeline.
  --test-category CATEGORY
                        test category result. Default: functional
  --test-namespace NAMESPACE
                        test namespace. Default: gating.
  --test-type TYPE      test type. Default: tier1.
  --test-status {complete,error,running}
                        test status.
  --test-result {passed,failed}
                        test result.
  --certificate CERTIFICATE
                        Certificate file (pem) to send messages through UMB. Can also set via UMB_CERTIFICATE environment variable.
  --dry-run             Only print the message, does not send to UMB.
```

## Testing

From the repository root, run tox tests:
`podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production tox`
