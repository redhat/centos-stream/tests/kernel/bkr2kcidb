# Send slack message to a slack channel using webhook

The tool requires the message to be a file.

Also, it expects the enviroment variable `SLACK_WEBHOOK_URL` to be set.

## Creating the webhook url

Open the slack app settings, under `Features` select `Incoming Webhooks`.

Then click on button `Add New Webhook to Workspace`, in the next page select
the channel to receive the messages.

Link to the app settings is <https://api.slack.com/apps/${APP_ID}>

## Help

```shell
send_slack_notification -h
usage: send_slack_notification.py [-h] --message-file MESSAGE_FILE

Send message to channel using webhook

options:
  -h, --help            show this help message and exit
  --message-file MESSAGE_FILE
```
